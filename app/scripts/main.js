$(document).ready(function() {
    // mask phone
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone


    // anchor
    $(".anchor").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top + 40}, 1000);
    });
    // anchor


    // slider
    $('.single-item').slick({
        arrows: true,
        dots: false
    });
    // slider


    // sidemenu
    $('.mainMenuBtn').click(function (){
        $(this).parents().find('.mainMenu').css('left', '0');
    });
    $('.mainMenuClose').click(function (){
        $(this).parent().css('left', '-105%');
    });
    // sidemenu


    // animation number
    $('.number').appear(function() {
        var dataN = $(this).attr('data-number');
        $(this).animate({ num: dataN}, {
            duration: 3500,
            step: function (num){
                this.innerHTML = (num).toFixed(0)
            }
        });
    });
    // animation number
});

